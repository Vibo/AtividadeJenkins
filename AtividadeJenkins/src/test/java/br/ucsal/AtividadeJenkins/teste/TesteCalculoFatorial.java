package br.ucsal.AtividadeJenkins.teste;

import org.junit.Test;
import br.ucsal.AtividadeJenkins.calculo.CalculoFatorial;
import org.junit.Assert;;

public class TesteCalculoFatorial extends CalculoFatorial{
	
	@Test
	public void testeFatorial(){
		//5! = 120
		int valorEsperado = 120;
		int valorAtual = CalculoFatorial.fatorial(5);
		Assert.assertEquals(valorEsperado, valorAtual);
	}
	
}